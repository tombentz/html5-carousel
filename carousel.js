/*
* HTML5/CSS3 Carousel
* Copyright © 2012 Thomas Bentz
* Email: digitalblur1@gmail.com
*/

function Carousel(){
	var _this = this;
	this.maxPanels = 10;
	this.panelList = {};
	this.startPanel = 0;
	this.inactivePanelWidth = 300;
	this.inactivePanelHeight = 300;
	this.activePanelWidth = 400;
	this.activePanelHeight = 400;
	this.panelSpacing = 100;

	// setup carousel width
	this.carouselWidth = ((this.maxPanels-1) * this.inactivePanelWidth) + (this.maxPanels * this.panelSpacing) + this.activePanelWidth;

	// initialize carousel and panels
	this.init = function(){
		var carousel = document.getElementById('carousel');
		carousel.style.width = this.carouselWidth + 'px';
		carousel.style.top = (window.innerHeight/2) - (this.activePanelHeight/2)  + 'px';
		this.createPanels(); // create panels
		// init the start panel
		var offset = this.getOffset(this.startPanel);
		this.activate(this.startPanel, offset);
		this.bindKeys();
	}

	// bind our left and right arrow keys
	this.bindKeys = function(){
		document.onkeydown = function(evt) {
			evt = evt || window.event;
			switch (evt.keyCode) {
				case 37:
					_this.moveLeft();
					break;
				case 39:
					_this.moveRight();
					break;
			}
		};
	}

	this.createPanels = function(){
		for(var i=0;i<this.maxPanels;i++){
			var li = document.createElement('li');
			var span = document.createElement('span');
			span.id = i;
			span.className = 'panel';
			this.panelList[i] = 'inactive';
			li.style.paddingRight = this.panelSpacing + 'px';
			li.appendChild(span);
			document.getElementById('carousel').appendChild(li);
		}
	}

	// setup panel to be active
	this.activate = function(id, newOffset){
		document.getElementById('carousel').style.right = newOffset + 'px';

		// make current panel inactive
		var activePanel = this.getActivePanel();
		if(activePanel){
	      this.deactivate(activePanel);
		}

		// activate new panel and zoom in
		this.panelList[id] = 'active';
		document.getElementById(id).style.width = this.activePanelWidth + 'px';
		document.getElementById(id).style.height = this.activePanelWidth + 'px';
	}

	// deactivate panel and zoom out
	this.deactivate = function(id){
		this.panelList[id] = 'inactive';
		document.getElementById(id).style.width = this.inactivePanelWidth + 'px';
		document.getElementById(id).style.height = this.inactivePanelWidth + 'px';
	}

	// move panels to the left
	this.moveLeft = function(){
		var activePanel = this.getActivePanel();
		if(activePanel != 0){
			this.deactivate(activePanel);
			var offset = this.getOffset(activePanel-1);
			this.activate(activePanel - 1, offset);
		}
	}

	// move panels to the right
	this.moveRight = function(){
		var activePanel = this.getActivePanel();
		if(activePanel != (_this.maxPanels-1)){
			this.deactivate(activePanel);
			var offset = this.getOffset(activePanel+1);
			this.activate(activePanel + 1, offset);
		}
	}

	// get position of new panel
	this.getOffset = function(id){
		var offset = (id * this.activePanelWidth) - (window.innerWidth/2) + (this.activePanelWidth/2);
		return offset;
	}

	// get the active panel
	this.getActivePanel = function(){
		for (var activeId in this.panelList) {
			if(this.panelList[activeId] == 'active'){
				return activeId * 1; // ensure a number
			}
		}
		return false;
	}
};

// init carousel on window load
var carousel = new Carousel();
window.onload = function(){
	carousel.init();
}
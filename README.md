# HTML5/CSS3 Carousel
* Open in an HTML5 compatible browser
* Use the left/right arrows to scroll the panels

This is a dynamically rendered Carousel where the size, color and number of panels can be changed using variables.

Contents would be loaded from an API

Copyright © 2012 Thomas Bentz
email: digitalblur1@gmail.com